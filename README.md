# saltstack

This project builds a new version of the SaltStack salt-master container to extend the container with additional utilities (primarily GPG).

In addition, useful utilities and code are kept here to assist in the administration and maintenance of a Salt master.

## Using the salt-master container

The salt-master container is located at

    registry.gitlab.com/wt0f/saltstack:<VERSION>

This container will then follow the configuration setup or the `saltstack/salt`
container up at hub.docker.com.

## Building the salt-master container

The pipeline will build the latest container for the Salt version defined
in the `variables` section. If another version of Salt needs to be built,
then define the variable `SALT_VERSION` when the pipeline is manually
triggered.
